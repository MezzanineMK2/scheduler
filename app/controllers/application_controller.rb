class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale
   
  def after_sign_in_path_for(resource)
    if current_user.teacher
      "/weekly/" + current_user.teacher.id.to_s + "?date=" + Date.today.to_s
    else
      "/"
    end
  end

  def set_locale
    if current_user
      if current_user.language
        I18n.locale = current_user.language
      else
        I18n.locale = I18n.default_locale  
      end
    else
      I18n.locale = I18n.default_locale
    end
    puts "LOCALE: " + I18n.locale.to_s
  end
end
