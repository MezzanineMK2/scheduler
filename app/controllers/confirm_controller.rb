class ConfirmController < ApplicationController
  def ok
    n = Notification.find(params[:n])
    if n
      puts Notification::Status::Confirmed
      puts n.status
      if n.status.to_s == Notification::Status::Confirmed.to_s
        render :text => "You have already confirmed this message."
      else
        n.confirmed_at = Time.now.getgm
        n.status = Notification::Status::Confirmed
        n.save
        render :text => "Message confirmed! Thank you very much!"
      end
    else
      render :text => "Notification not found!"
    end
  end

  def staff_ok
    n = Notification.find(params[:n])
    if n
      puts Notification::Status::Confirmed
      puts n.status
      if n.status.to_s == Notification::Status::Confirmed.to_s

      else
        n.confirmed_at = Time.now.getgm
        n.status = Notification::Status::Confirmed
        n.save
      end
    end
    respond_to do |format|
      format.html { redirect_to notifications_url }
    end
  end

  def ng
  end
end
