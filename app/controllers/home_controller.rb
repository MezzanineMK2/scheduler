class HomeController < ApplicationController
  before_filter :authenticate_user!

  def index
    @date = params[:date]
    @time = DateTime.now.to_time
    if !@date then @date = DateTime.now end
    @date = @date.to_date
    @teachers = Teacher.order("name ASC").all
    @user = current_user
    @is_staff = current_user.is_staff?
  end
end
