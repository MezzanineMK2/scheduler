class LessonsController < ApplicationController
  before_filter :authenticate_user!
  # GET /lessons
  # GET /lessons.json
  def index
    @lessons = Lesson.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lessons }
    end
  end

  # GET /lessons/1
  # GET /lessons/1.json
  def show
    @lesson = Lesson.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: {"lesson" => @lesson, "teacher" => Teacher.find(@lesson.teacher_id)} }
    end
  end

  # GET /lessons/new
  # GET /lessons/new.json
  def new
    @lesson = Lesson.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lesson }
    end
  end

  # GET /lessons/1/edit
  def edit
    @lesson = Lesson.find(params[:id])
  end

  # POST /lessons
  # POST /lessons.json
  def create
    @lesson = Lesson.new(params[:lesson])

    respond_to do |format|
      if @lesson.save
        format.html { redirect_to @lesson, notice: 'Lesson was successfully created.' }
        format.json { render json: @lesson, status: :created, location: @lesson }
      else
        format.html { render action: "new" }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lessons/1
  # PUT /lessons/1.json
  def update
    @lesson = Lesson.find(params[:id])

    respond_to do |format|
      if @lesson.update_attributes(params[:lesson])
        format.html { redirect_to @lesson, notice: 'Lesson was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lessons/1
  # DELETE /lessons/1.json
  def destroy
    @lesson = Lesson.find(params[:id])
    @lesson.destroy

    respond_to do |format|
      format.html { redirect_to lessons_url }
      format.json { render json: {"status" => 1, "msg" => 'Lesson Deleted'} }
    end
  end

  # GET /lessons/1/cancel
  def cancel
    @lesson = Lesson.find(params[:id])
    teacher = @lesson.teacher
    staff_name = params[:staff_name]
    @newstatus = params[:status]
    mail_to = {}
    status = 0
    if @lesson
      @lesson.status = @newstatus
      if @lesson.save
        status = 1

        teacher = @lesson.teacher
        shouldSendMail = params[:sendemail]
        msg = "Class cancelled successfully. Please notify teacher by phone or e-mail."
        ScheduleMailer.cancellation_notice_jp(teacher, @lesson, "Status goes here", staff_name).deliver
        if shouldSendMail == "true" and !teacher.email.nil?
          notification = Notification.new
          notification.lesson_id = @lesson.id
          notification.teacher_id = teacher.id
          notification.notification_type = Notification::Type::Cancellation
          notification.status = Notification::Status::Unconfirmed
          notification.save
          mail_to_address = teacher.email
          mail_to_subject = URI.escape("IAY Cancellation Notice - #{@lesson.name}")
          mail_to_message = ""
          mail_to_message += "#{teacher.name},\n"
          mail_to_message += "\n"
          mail_to_message += "One of your lessons has been cancelled for #{@lesson.date}:\n"
          mail_to_message += "\n"
          mail_to_message += "NAME: #{@lesson.name}\n"
          mail_to_message += "TIME: #{@lesson.start_time.strftime("%H:%M")} - #{@lesson.finish_time.strftime("%H:%M")}\n"
          mail_to_message += "\n"
          mail_to_message += "Please click the link below to confirm:\n"
          mail_to_message += "#{Yetting.base_url}confirm/ok?n=#{notification.id}\n"
          mail_to_message = URI.escape(mail_to_message)
          ScheduleMailer.cancellation_notice(teacher, @lesson, "Status goes here", notification.id, false).deliver
          ScheduleMailer.cancellation_notice(teacher, @lesson, "Status goes here", notification.id, true).deliver
          msg = "Class cancelled successfully. E-mail sent!"
        end          
        
      end
    end
    respond_to do |format|
      format.json { render json: {"status" => status, "msg" => msg, "mail_to_address" => mail_to_address, "mail_to_subject" => mail_to_subject, "mail_to_message" => mail_to_message} }
    end
  end

  # GET /lessons/1/sub
  def sub
    @lesson = Lesson.find(params[:id])
    @newteacher = params[:teacher_id]
    status = 0
    msg = "Switching this lesson to a substitute teacher..."
    if @lesson
      if @lesson.original_teacher_id.to_i == @newteacher.to_i
        msg = "Returning this lesson to its original teacher..."
        @lesson.original_teacher_id = nil
        @lesson.teacher_id = @newteacher
      else          
        @lesson.original_teacher_id = @lesson.teacher_id
        @lesson.teacher_id = @newteacher
      end
      if @lesson.save
        status = 1
      end
    end
    respond_to do |format|
      format.json { render json: {"status" => status, "msg" => msg} }
    end
  end

  # GET /lessons/add
  def add
    @lesson = Lesson.new
    @lesson.teacher_id = params[:teacher_id]
    @lesson.status = Lesson::Status::OK
    datetime = DateTime.parse(params[:date].to_s + "T" + params[:start_time] + ":00+00:00")
    @lesson.date = datetime.to_date
    @lesson.start_time = datetime
    #@lesson.length = params[:length].to_i
    @lesson.finish_time = datetime + params[:length].to_i.minute
    @lesson.name = params[:name]

    status = 0
    if @lesson.save
      status = 1
    end

    respond_to do |format|
      format.json { render json: {"status" => status} }
    end
  end


end
