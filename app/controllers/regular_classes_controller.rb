class RegularClassesController < ApplicationController
  before_filter :authenticate_user!
  helper LaterDude::CalendarHelper
 # GET /regular_classes
  # GET /regular_classes.json
  def index
    @sort = params[:sort]
    case params[:sort]
    when "day"
      if params[:schedule_type]
        @regular_classes = RegularClass.order("day ASC").order("start_time ASC").find_all_by_schedule_type_id(params[:schedule_type])
      else
        @regular_classes = RegularClass.order("day ASC").order("start_time ASC").all
      end
    else
      @sort = "name"
      if params[:schedule_type]
        @regular_classes = RegularClass.order("name ASC").find_all_by_schedule_type_id(params[:schedule_type])
      else
        @regular_classes = RegularClass.order("name ASC").all
      end
    end


    @schedule_type = params[:schedule_type]

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @regular_classes }
    end
  end

  # GET /regular_classes/1
  # GET /regular_classes/1.json
  def show
    @regular_class = RegularClass.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @regular_class }
    end
  end

  # GET /regular_classes/new
  # GET /regular_classes/new.json
  def new
    @regular_class = RegularClass.new

    teacher = []
    Teacher.all.each { |teach| teacher.push([teach.name, teach.id])}
    @teachers = teacher

    schedule_type = []
    ScheduleType.all.each { |sched| schedule_type.push([sched.name, sched.id])}
    @schedule_types = schedule_type

    @default_schedule_type = params[:schedule_type]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @regular_class }
    end
  end

  # GET /regular_classes/1/edit
  def edit
    teacher = []
    Teacher.all.each { |teach| teacher.push([teach.name, teach.id])}
    @teachers = teacher

    schedule_type = []
    ScheduleType.all.each { |sched| schedule_type.push([sched.name, sched.id])}
    @schedule_types = schedule_type

    @regular_class = RegularClass.find(params[:id])
  end

  # POST /regular_classes
  # POST /regular_classes.json
  def create
    @regular_class = RegularClass.new(params[:regular_class])

    respond_to do |format|
      if @regular_class.save
        format.html { redirect_to schedule_lessons_url(@regular_class), notice: 'Regular class was successfully created.' }
        format.json { render json: @regular_class, status: :created, location: @regular_class }
      else
        format.html { render action: "new" }
        format.json { render json: @regular_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /regular_classes/1
  # PUT /regular_classes/1.json
  def update
    @regular_class = RegularClass.find(params[:id])
    foo = params[:foo]

    respond_to do |format|
      if @regular_class.update_attributes(params[:regular_class])
        format.html { redirect_to @regular_class, notice: 'Regular class was successfully updated. ' + foo}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @regular_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /regular_classes/1/schedule_lessons
  def schedule_lessons
    @regular_class = RegularClass.find(params[:id])
    @schedule_type = @regular_class.schedule_type
    @holidays = []
    Holiday.where("schedule_type_id = ?", @schedule_type.id).pluck("Date(date)").each { |holiday| @holidays.push(holiday.to_s)}
    @lessons = []
    Lesson.where("regular_class_id = ?", params[:id]).pluck("Date(date)").each {|lesson| @lessons.push(lesson.to_s)}
  end

  # POST /regular_classes/1/create_lessons
  def create_lessons
    @regular_class = RegularClass.find(params[:id])
    dates = params[:dates]
    logger.info("Starting log...")
    Lesson.where("regular_class_id = ?", params[:id]).each do |l|
      logger.info("Destroying lesson for: " + l.date.to_s)
      l.destroy
    end

    lessons = 0
    if dates then 
      dates.each do |d|
        lesson = Lesson.new(:date => d, 
          :regular_class_id => params[:id], 
          :start_time => @regular_class.start_time, 
          :finish_time => @regular_class.finish_time, 
          :teaching_time => @regular_class.teaching_time, 
          :non_teaching_time => @regular_class.non_teaching_time, 
          :teacher_id => @regular_class.teacher_id, 
          :name => @regular_class.name,
          :status => Lesson::Status::OK)
        if lesson.save then 
          lessons = lessons + 1
        end
      end
    end

    respond_to do |format|
      format.html { redirect_to regular_classes_url(:schedule_type => @regular_class.schedule_type_id), :flash => { :alert => "add_lessons", :class_name => @regular_class.name, :count => lessons.to_s }}
    end
  end

  # DELETE /regular_classes/1
  # DELETE /regular_classes/1.json
  def destroy
    @regular_class = RegularClass.find(params[:id])
    @regular_class.destroy

    respond_to do |format|
      format.html { redirect_to regular_classes_url }
      format.json { head :no_content }
    end
  end
end
