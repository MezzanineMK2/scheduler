class ScheduleTypesController < ApplicationController
  before_filter :authenticate_user!
  helper LaterDude::CalendarHelper
  # GET /schedule_types
  # GET /schedule_types.json
  def index
    @schedule_types = ScheduleType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @schedule_types }
    end
  end

  # GET /schedule_types/1
  # GET /schedule_types/1.json
  def show
    @schedule_type = ScheduleType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @schedule_type }
    end
  end

  # GET /schedule_types/new
  # GET /schedule_types/new.json
  def new
    @schedule_type = ScheduleType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @schedule_type }
    end
  end

  # GET /schedule_types/1/edit
  def edit
    @schedule_type = ScheduleType.find(params[:id])
    @show_calendar = true
    @holidays = []
    Holiday.where("schedule_type_id = ?", params[:id]).pluck("Date(date)").each { |holiday| @holidays.push(holiday.to_s)}
    @holidays.each do |h|
      puts "holiday: " + h
    end
  end

  # GET /schedule_types/1/set_holidays
  def set_holidays
    @schedule_type = ScheduleType.find(params[:id])
  end


  # POST /schedule_types
  # POST /schedule_types.json
  def create
    @schedule_type = ScheduleType.new(params[:schedule_type])

    respond_to do |format|
      if @schedule_type.save
        format.html { redirect_to schedule_types_url, :flash => { :alert => "create" } }
        format.json { render json: @schedule_type, status: :created, location: @schedule_type }
      else
        format.html { render action: "new" }
        format.json { render json: @schedule_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /schedule_types/1
  # PUT /schedule_types/1.json
  def update
    @schedule_type = ScheduleType.find(params[:id])
    dates = params[:dates]

    Holiday.where("schedule_type_id = ?", params[:id]).each do |h|
      puts "Deleting holiday: " + h.date.to_s
      h.destroy
    end
    dates.each do |d|
      holiday = Holiday.new(:date => d, :schedule_type_id => params[:id])
      puts "Saving holiday for " + d.to_s
      if holiday.save
        puts "Saved"
      end
    end
    respond_to do |format|
      if @schedule_type.update_attributes(params[:schedule_type])
        format.html { redirect_to @schedule_type, notice: 'Schedule type was successfully updated. '}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @schedule_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedule_types/1
  # DELETE /schedule_types/1.json
  def destroy
    @schedule_type = ScheduleType.find(params[:id])
    @schedule_type.destroy

    respond_to do |format|
      format.html { redirect_to schedule_types_url, :flash => { :alert => "delete" } }
      format.json { head :no_content }
    end
  end
end
