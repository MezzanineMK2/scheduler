class TeachersController < ApplicationController
  before_filter :authenticate_user!
  require 'base64'
  # GET /teachers
  # GET /teachers.json
  def index
    @teachers = Teacher.order("name ASC").all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @teachers }
    end
  end

  # GET /teachers/1
  # GET /teachers/1.json
  def show
    @teacher = Teacher.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @teacher }
      format.csv { send_data @teacher.schedule_csv, :filename => (@teacher.name + " Schedule.csv")}
    end
  end

  # GET /teachers/new
  # GET /teachers/new.json
  def new
    @teacher = Teacher.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @teacher }
    end
  end

  # GET /teachers/1/edit
  def edit
    @teacher = Teacher.find(params[:id])
  end

  # POST /teachers
  # POST /teachers.json
  def create
    @teacher = Teacher.new(params[:teacher])

    respond_to do |format|
      if @teacher.save
        format.html { redirect_to @teacher, notice: 'Teacher was successfully created.' }
        format.json { render json: @teacher, status: :created, location: @teacher }
      else
        format.html { render action: "new" }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /teachers/1
  # PUT /teachers/1.json
  def update
    @teacher = Teacher.find(params[:id])

    respond_to do |format|
      if @teacher.update_attributes(params[:teacher])
        format.html { redirect_to @teacher, notice: 'Teacher was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teachers/1
  # DELETE /teachers/1.json
  def destroy
    @teacher = Teacher.find(params[:id])
    @teacher.destroy

    respond_to do |format|
      format.html { redirect_to teachers_url }
      format.json { head :no_content }
    end
  end

  # GET /teachers/1/send_test_mail
  def send_test_mail
    @teacher = Teacher.find(params[:id])

    status = 0
    if ScheduleMailer.test_notice(@teacher).deliver
      status = 1
    end

    respond_to do |format|
      format.json { render json: {"status" => status} }
    end
  end

  # POST /teachers/1/send_weekly_schedule_mail
  def send_weekly_schedule_mail
    @teacher = Teacher.find(params[:id])
    @img_url = params[:img_url]
    image = Base64.decode64(@img_url)
    File.open("#{Rails.root}/test.png", 'wb') do|f|
      f.write(image)
    end
    status = 0
    if ScheduleMailer.test_notice(@teacher).deliver
      status = 1
    end

    respond_to do |format|
      format.json { render json: {"status" => status} }
    end
  end

  # GET /teachers/1/availability
  def availability
    @teacher = Teacher.find(params[:id])
    @off_times = OffTime.order("day ASC").order("start_time ASC").find_all_by_teacher_id(params[:id])
  end

  # POST /teachers/1/set_availability
  def set_availability
    @teacher = Teacher.find(params[:id])

    start_times = params[:start_times]
    finish_times = params[:finish_times]
    days = params[:days]

    OffTime.where("teacher_id = ?", params[:id]).each do |o|
      puts "Deleting off time: " + o.start_time.to_s + " - " + o.finish_time.to_s
      o.destroy
    end
    start_times.zip(finish_times, days).each do |sf|
      start_time = sf[0].split(":")
      finish_time = sf[1].split(":")
      off_time = OffTime.new(:start_time => Time.utc(2000, 1, 1, start_time[0], start_time[1], 0), :finish_time => Time.utc(2000, 1, 1, finish_time[0], finish_time[1], 0), :day => sf[2], :teacher_id => params[:id])
      puts "Saving time off for " + sf[0] + " - " + sf[1] + " (day " + sf[2] + ")"
      if off_time.save
        puts "Saved"
      end
    end
    respond_to do |format|
      format.html { redirect_to teachers_url }
      format.json { head :no_content }
    end
  end

end
