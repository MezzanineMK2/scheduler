class WeeklyScheduleController < ApplicationController
  before_filter :authenticate_user!
  def weekly
    @teacher = Teacher.find(params[:id])
    @date = DateTime.now + 7
    if params[:date] then @date = params[:date].to_date end
    @date = @date - @date.wday + 1

    @lessons = {}
    @off_times = {}
    6.times do |i|
      d = @date + i
      @lessons[d.strftime("%m/%d (%^a)")] = @teacher.lessons_on(d)
      @off_times[d.strftime("%m/%d (%^a)")] = @teacher.off_times_on(d)
    end
  end
end
