# encoding: utf-8
class ScheduleMailer < ActionMailer::Base
  default from: "scheduler@myiay.com"

  def test_notice(teacher)
    @teacher = teacher
    mail(:to => "#{teacher.name} <#{teacher.email}>", :subject => "Hi there!")
  end

  def cancellation_notice(teacher, lesson, type, notification_id, is_cc)
    @teacher = teacher
    @lesson = lesson
    @type = type
    @notification_id = notification_id
    if is_cc
      mail(:to => "Head Teacher <#{Yetting.head_teacher_email}>", :subject => "IAY Cancellation Notice (#{teacher.name}) - #{lesson.name}")
    else
      mail(:to => "#{teacher.name} <#{teacher.email}>", :subject => "IAY Cancellation Notice - #{lesson.name}")
    end
  end

  def cancellation_notice_jp(teacher, lesson, type, staff_name)
    @teacher = teacher
    @lesson = lesson
    @type = type
    @staff_name = staff_name
    mail(:to => "IAY Reception <#{Yetting.staff_email}>", :cc => Yetting.staff_cc, :subject => "レッスンキャンセル通知 - #{lesson.date.strftime("%m月%d日")} '#{lesson.name}'")
  end
end
