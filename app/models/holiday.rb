class Holiday < ActiveRecord::Base
  attr_accessible :date, :schedule_type_id

  belongs_to :schedule_type
end
