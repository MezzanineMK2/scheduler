class Lesson < ActiveRecord::Base
  module Status
    OK = "1" # Lesson::Status::OK
    CancelledOnPreviousDay = "2"
    CancelledBeforeArrival = "3"
    CancelledAfterArrival = "4"
    NoShow = "5"
  end
  attr_accessible :finish_time, :location, :name, :start_time, :status, :original_teacher_id, :regular_class_id, :date, :teacher_id, :teaching_time, :non_teaching_time

  belongs_to :regular_class
  belongs_to :teacher

  def length
    ((finish_time.to_datetime - start_time.to_datetime) * 1440).to_i
  end

  def start_5minutes
    ((start_time.hour - 9) * 12) + (start_time.min / 5)
  end

  def tooltip_text
    text = name + " (" + start_time.strftime('%H:%M') + "~" + finish_time.strftime('%H:%M') + ")"
    text = text + "<br>Cancelled" unless status == Lesson::Status::OK
    if original_teacher_id
      text = text + "<br>Sub for " + Teacher.find(original_teacher_id).name
    end
    text
  end
end

