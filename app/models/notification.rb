class Notification < ActiveRecord::Base
  module Status
    Unconfirmed = "1"   # Notification::Status::Unconfirmed
    Confirmed = "2"     # Notification::Status::Confirmed
  end

  module Type
    Cancellation = "1"  # Notification::Type::Cancellation
    Addition = "2"      # Notification::Type::Addition
  end

  attr_accessible :confirmed_at, :lesson_id, :notification_type, :status, :teacher_id

end
