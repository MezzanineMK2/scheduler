class OffTime < ActiveRecord::Base
  attr_accessible :day, :finish_time, :start_time, :teacher_id

  belongs_to :teacher

  def length
    ((finish_time.to_datetime - start_time.to_datetime) * 1440).to_i
  end

  def start_5minutes
    ((start_time.hour - 9) * 12) + (start_time.min / 5)
  end
  
end
