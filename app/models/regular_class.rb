class RegularClass < ActiveRecord::Base
  attr_accessible :day, :finish_time, :length, :location, :name, :start_time, :textbook, :teacher_id, :schedule_type_id, :teaching_time, :non_teaching_time
  
  belongs_to :teacher
  belongs_to :schedule_type
  has_many :lessons, :dependent => :destroy

  def conflicts?
    this_start = start_time.strftime("%H%M").to_i
    this_finish = finish_time.strftime("%H%M").to_i
    RegularClass.where("teacher_id = ? AND day = ? AND day != -1 AND id != ?", teacher_id, day, id).each do |c|
      puts "checking " + c.name
      other_start = c.start_time.strftime("%H%M").to_i
      other_finish = c.finish_time.strftime("%H%M").to_i
      if this_start.between?(other_start, other_finish) or this_finish.between?(other_start, other_finish)
        return true
      end
    end
    return false
  end
end
