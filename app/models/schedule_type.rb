class ScheduleType < ActiveRecord::Base
  attr_accessible :name, :start_date, :end_date
  has_many :holidays
  has_many :regular_classes
end
