class Teacher < ActiveRecord::Base
  attr_accessible :name, :id, :email, :message_preference
  
  has_many :regular_classes
  has_many :lessons, :through => :regular_classes
  has_many :off_times

  def lessons_on(date)
    Lesson.where("date = ? AND teacher_id = ?", date, id)
  end

  def off_times_on(date)
    OffTime.where("day = ? AND teacher_id = ?", date.cwday, id)
  end

  def schedule_csv(options = {})
    year_lessons = Lesson.where("teacher_id = ?", id).order("date ASC").order("start_time ASC");
    CSV.generate(options) do |csv|
      csv << ["Subject", "Start Date", "Start Time", "End Date", "End Time"]
      year_lessons.each do |l|
        csv << [l.name, l.date, l.start_time.strftime('%H:%M'), l.date, l.finish_time.strftime('%H:%M')]
      end
    end
  end
end
