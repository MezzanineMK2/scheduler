class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :admin_flag, :teacher_id, :language
  # attr_accessible :title, :body

  def is_staff?
    #email.include? "@myiay.com"
    admin_flag || email == "alex@myiay.com"
  end

  def teacher    
    Teacher.where("email = ?", email).first
  end
end
