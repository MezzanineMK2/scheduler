class CreateRegularClasses < ActiveRecord::Migration
  def change
    create_table :regular_classes do |t|
      t.integer :length
      t.time :start_time
      t.time :finish_time
      t.string :textbook
      t.string :name
      t.string :location
      t.integer :day

      t.timestamps
    end
  end
end
