class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.time :start_time
      t.time :finish_time
      t.string :name
      t.string :status
      t.string :location

      t.timestamps
    end
  end
end
