class AddOriginalTeacherToLessons < ActiveRecord::Migration
  def change
    add_column :lessons, :original_teacher_id, :integer
  end
end
