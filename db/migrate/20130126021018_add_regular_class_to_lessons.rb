class AddRegularClassToLessons < ActiveRecord::Migration
  def change
    add_column :lessons, :regular_class_id, :integer
  end
end
