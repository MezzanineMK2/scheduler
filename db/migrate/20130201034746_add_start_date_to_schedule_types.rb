class AddStartDateToScheduleTypes < ActiveRecord::Migration
  def change
    add_column :schedule_types, :start_date, :date
    add_column :schedule_types, :end_date, :date
  end
end
