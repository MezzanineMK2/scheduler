class AddTeachingTimeAndNonTeachingTimeToRegularClass < ActiveRecord::Migration
  def change
    add_column :regular_classes, :teaching_time, :integer
    add_column :regular_classes, :non_teaching_time, :integer
  end
end
