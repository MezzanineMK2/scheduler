class AddTeachingTimeAndNonTeachingTimeToLesson < ActiveRecord::Migration
  def change
    add_column :lessons, :teaching_time, :integer
    add_column :lessons, :non_teaching_time, :integer
  end
end
