class AddEmailAndMessagePreferenceToTeachers < ActiveRecord::Migration
  def change
    add_column :teachers, :email, :string
    add_column :teachers, :message_preference, :integer, :default => 0
  end
end
