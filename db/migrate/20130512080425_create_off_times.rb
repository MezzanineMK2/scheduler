class CreateOffTimes < ActiveRecord::Migration
  def change
    create_table :off_times do |t|
      t.integer :teacher_id
      t.integer :day
      t.time :start_time
      t.time :finish_time

      t.timestamps
    end
  end
end
