class AddAdminFlagAndTeacherIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :admin_flag, :boolean
    add_column :users, :teacher_id, :integer
  end
end
