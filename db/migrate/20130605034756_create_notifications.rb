class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :teacher_id
      t.datetime :confirmed_at
      t.integer :lesson_id
      t.integer :notification_type
      t.integer :status

      t.timestamps
    end
  end
end
