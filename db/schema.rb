# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130607102316) do

  create_table "holidays", :force => true do |t|
    t.date     "date"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "schedule_type_id"
  end

  create_table "lessons", :force => true do |t|
    t.time     "start_time"
    t.time     "finish_time"
    t.string   "name"
    t.string   "status"
    t.string   "location"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "original_teacher_id"
    t.integer  "regular_class_id"
    t.date     "date"
    t.integer  "teacher_id"
    t.integer  "teaching_time"
    t.integer  "non_teaching_time"
  end

  create_table "notifications", :force => true do |t|
    t.integer  "teacher_id"
    t.datetime "confirmed_at"
    t.integer  "lesson_id"
    t.integer  "notification_type"
    t.integer  "status"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "off_times", :force => true do |t|
    t.integer  "teacher_id"
    t.integer  "day"
    t.time     "start_time"
    t.time     "finish_time"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "regular_classes", :force => true do |t|
    t.integer  "length"
    t.time     "start_time"
    t.time     "finish_time"
    t.string   "textbook"
    t.string   "name"
    t.string   "location"
    t.integer  "day"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "teacher_id"
    t.integer  "schedule_type_id"
    t.integer  "teaching_time"
    t.integer  "non_teaching_time"
  end

  create_table "schedule_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.date     "start_date"
    t.date     "end_date"
  end

  create_table "teachers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "email"
    t.integer  "message_preference", :default => 0
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "admin_flag"
    t.integer  "teacher_id"
    t.string   "language"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
