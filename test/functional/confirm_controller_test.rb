require 'test_helper'

class ConfirmControllerTest < ActionController::TestCase
  test "should get ok" do
    get :ok
    assert_response :success
  end

  test "should get ng" do
    get :ng
    assert_response :success
  end

end
