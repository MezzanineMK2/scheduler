require 'test_helper'

class RegularClassesControllerTest < ActionController::TestCase
  setup do
    @regular_class = regular_classes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:regular_classes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create regular_class" do
    assert_difference('RegularClass.count') do
      post :create, regular_class: { day: @regular_class.day, finish_time: @regular_class.finish_time, length: @regular_class.length, location: @regular_class.location, name: @regular_class.name, start_time: @regular_class.start_time, textbook: @regular_class.textbook }
    end

    assert_redirected_to regular_class_path(assigns(:regular_class))
  end

  test "should show regular_class" do
    get :show, id: @regular_class
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @regular_class
    assert_response :success
  end

  test "should update regular_class" do
    put :update, id: @regular_class, regular_class: { day: @regular_class.day, finish_time: @regular_class.finish_time, length: @regular_class.length, location: @regular_class.location, name: @regular_class.name, start_time: @regular_class.start_time, textbook: @regular_class.textbook }
    assert_redirected_to regular_class_path(assigns(:regular_class))
  end

  test "should destroy regular_class" do
    assert_difference('RegularClass.count', -1) do
      delete :destroy, id: @regular_class
    end

    assert_redirected_to regular_classes_path
  end
end
