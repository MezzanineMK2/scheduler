require 'test_helper'

class ScheduleTypesControllerTest < ActionController::TestCase
  setup do
    @schedule_type = schedule_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:schedule_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create schedule_type" do
    assert_difference('ScheduleType.count') do
      post :create, schedule_type: { name: @schedule_type.name }
    end

    assert_redirected_to schedule_type_path(assigns(:schedule_type))
  end

  test "should show schedule_type" do
    get :show, id: @schedule_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @schedule_type
    assert_response :success
  end

  test "should update schedule_type" do
    put :update, id: @schedule_type, schedule_type: { name: @schedule_type.name }
    assert_redirected_to schedule_type_path(assigns(:schedule_type))
  end

  test "should destroy schedule_type" do
    assert_difference('ScheduleType.count', -1) do
      delete :destroy, id: @schedule_type
    end

    assert_redirected_to schedule_types_path
  end
end
